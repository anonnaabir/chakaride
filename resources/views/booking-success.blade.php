<livewire:header />

<div class="grid h-screen place-items-center bg-rose-600 text-white h-screen w-full">

<div class="checkout-container bg-white justify-center p-12">
    
    <lottie-player class="sm:w-full md:w-2/5" autoplay loop mode="normal"
    src="https://assets10.lottiefiles.com/packages/lf20_8NYY2Y.json"
    >
    </lottie-player>

    <h1 class="sm:text-2xl md:text-3xl text-black text-center mt-4 mb-4">Congratualtion!</h1>
    <p class="sm:text-base md:text-2xl text-black text-center mt-2 mb-2">Your booking has received sucessfully.</p>
    <p class="sm:text-base md:text-2xl text-black text-center">Your booking id: <span class="text-red">{{$booking_id}}</span></p>
    <div class="sm:grid sm:grid-cols-1 md:flex md:justify-center">
    <a href="/dashboard" class="bg-rose-600 text-white text-center pt-4 pb-4 pl-8 pr-8 m-4">Dashboard</a>
    <a href="/" class="bg-rose-600 text-base text-white pt-4 pb-4 pl-4 pr-4 m-4">New Booking</a>
    </div>
</div>

</div>

<livewire:footer />