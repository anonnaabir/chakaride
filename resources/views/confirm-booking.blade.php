<livewire:header />
<div class="grid h-screen overflow-auto place-items-center bg-rose-600 text-white w-full"
x-data="
{
 checkoutStep1: true,
 checkoutStep2: false,
}"
>

<div class="checkout-container bg-white justify-center sm:p-6 md:p-12 sm:m-8 md:m-0 w-2/5">
<form action="/success">
{{ csrf_field() }}
<livewire:personal-details />
<livewire:booking-summary />
</form>

</div>

</div>

<livewire:footer />