<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Bookings') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-auto shadow-sm sm:rounded-lg">
                <div class="p-12 bg-white border-b border-gray-200">
                    <table class="w-full border border-indigo-600">
                    <tbody>
                    <thead class="border border-rose-600 bg-rose-600 text-white text-base">
                    <tr>
                    <th class="p-2">Booking Type</th>
                    <th class="p-2">Trip Type</th>
                    <th class="p-2">Selected Car</th>
                    <th class="p-2">Pickup Location</th>
                    <th class="p-2">Dropoff Location</th>
                    <th class="p-2">Pickup Date</th>
                    <th class="p-2">Pickup Time</th>
                    <th class="p-2">Dropoff Date</th>
                    <th class="p-2">Total Cost</th>
                    <th class="p-2">Booking Date</th>
                    </tr>
                </thead>
                    @foreach ($booking_data as $data)
                    <tr class="text-center border border-rose-600">
                    <td class="p-6 border border-rose-600">{{$data->booking_type}}</td>
                    <td class="p-6 border border-rose-600">{{$data->trip_type}}</td>
                    <td class="p-6 border border-rose-600">{{$data->car_model}}</td>
                    <td class="p-6 border border-rose-600">{{$data->pickup_location}}</td>
                    <td class="p-6 border border-rose-600">{{$data->dropoff_location}}</td>
                    <td class="p-6 border border-rose-600">{{$data->pickup_date}}</td>
                    <td class="p-6 border border-rose-600">{{$data->pickup_time}}</td>
                    <td class="p-6 border border-rose-600">{{$data->dropoff_date}}</td>
                    <td class="p-6 border border-rose-600">{{$data->total_cost}} TK</td>
                    <td class="p-6 border border-rose-600">{{ date('h:i:s a m/d/Y', strtotime($data->created_at)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
