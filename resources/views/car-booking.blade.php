<livewire:header />

<div class="grid h-screen overflow-auto place-items-center bg-rose-600 text-white w-full"
x-data="
{
 step1: true,
 step2: false,
 step3: false,
 step4: false,
 step5: false,
 bookingType: '',
}"
>

<div class="main-container bg-white justify-center sm:p-8 md:p-12 sm:m-8 md:m-0">
<livewire:booking-form />    

</div>


</div>
<livewire:footer />