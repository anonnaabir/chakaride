<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <div class="sm:grid md:flex sm:mt-2 md:mt-24 sm:mb-2 md:mb-24">
                    <div class="sm:w-full md:w-full mt-6 mb-6">
                            <a href="/" class="bg-black hover:bg-rose-600 text-white sm:text-base md:text-xl m-4 sm:p-4 md:p-24 rounded-md">New Booking</a>
                        </div>
                    
                        <div class="sm:w-full md:w-full mt-6 mb-6">
                            <a href="/my-profile" class="bg-black hover:bg-rose-600 text-white sm:text-base md:text-xl m-4 sm:p-4 md:p-24 rounded-md">My Profile</a>
                        </div>

                        <div class="w-full mt-6 mb-6">
                        <a href="/all-bookings" class="bg-black hover:bg-rose-600 text-white sm:text-base md:text-xl m-4 sm:p-4 md:p-24 rounded-md">My Bookings</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
