<livewire:header />

<div class="grid h-screen place-items-center bg-rose-600 text-white h-screen w-full"
x-data="
{
 checkoutStep1: true,
 checkoutStep2: false,
 loginpopup: false,
}"
>

<div class="checkout-container bg-white justify-center p-12">

<div>
 
<form action="/check-login" method="POST">
    @csrf
<label class="text-black" for="email">Email</label><br>
<input class="text-black w-full mt-4 mb-4" type="text" id="email" name="email" value=""><br>

<label class="text-black" for="customer_phone">Password</label><br>
<input class="text-black w-full mt-4 mb-4" type="password" id="password" name="password" value=""><br>

<button type="submit" class="bg-rose-600 text-white pt-3 pb-4 pl-20 pr-20 mt-4">Login</button>
</form>

</div>

</div>

</div>

<livewire:footer />