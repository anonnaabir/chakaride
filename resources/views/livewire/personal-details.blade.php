@if (Auth::check())

<div class="cr-booking-type pt-4" x-show="checkoutStep1">
    
<h1 class="sm:text-xl md:text-2xl text-black text-center">Enter Your Personal Details</h1>

<div class="sm:grid sm:grid-cols-1 md:grid md:grid-cols-1 pt-12 sm:pl-0 md:pl-8 sm:pr-0 md:pr-8">
    <div class="sm:w-full md:w-full">
    @unless (Auth::check())
    <p class="text-black">Already have an account? Login first.</p>
    @endunless
    <label class="text-black" for="name">Full Name</label><br>
    <input class="text-black w-full mt-4 mb-4 rounded-md" type="text" id="name" name="name" value="{{$user_data->name}}"><br>

    <label class="text-black" for="phone">Phone</label><br>
    <input class="text-black w-full mt-4 mb-4 rounded-md" type="text" id="phone" name="phone" value="{{$user_data->phone}}"><br>

    <label class="text-black" for="address">Address</label><br>
    <input class="text-black w-full mt-4 mb-4 rounded-md" type="text" id="address" name="address" value="{{$user_data->address}}"><br>

    <label class="text-black" for="email">Email</label><br>
    <input class="text-black w-full mt-4 mb-4 rounded-md" type="email" id="email" name="email" value="{{$user_data->email}}"><br>

    <label class="text-black" for="national_id">National ID</label><br>
    <input class="text-black w-full mt-4 mb-4 rounded-md" type="text" id="national_id" name="national_id" value="{{$user_data->national_id}}"><br>

    <label class="text-black" for="city">City</label><br>
    <select class="text-black w-full mt-4 mb-4 border border-rose-600 rounded-md" name="city" id="city">
    <option value="dhaka">Dhaka</option>
    </select><br>
    
    </div>

    <div class="flex justify-center">
    
    <button type="button" x-on:click="checkoutStep1 = false,checkoutStep2 = true"
    class="bg-rose-600 text-white pt-3 pb-4 sm:pl-8 md:pl-10 sm:pr-8 md:pr-10 sm:mt-4 md:mt-8 sm:mb-8 md:mb-4 rounded-md">Show Price</button>
    
    </div>

</div>

</div>

@else

<div class="cr-booking-type pt-4" x-show="checkoutStep1">
    
<h1 class="text-2xl text-black text-center">Enter Your Personal Details</h1>

<div class="grid place-items-center pt-12 sm:pl-2 md:pl-8 sm:pr-2 md:pr-8">
    <div class="sm:w-full md:w-4/5">

        @if ($errors->any())
        <div class="mt-4 mb-8" role="alert">
        <div class="bg-red-500 sm:text-sm md:text-base text-white font-bold rounded-t px-4 py-2">Something Wrong</div>
        <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 sm:text-sm md:text-base text-red-700">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="sm:mb-2 md:mb-2">{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        </div>

        <div class="mt-4 mb-8" role="alert">
        
        <div class="bg-yellow-100 border border-yellow-400 text-yellow-700 px-4 py-3 rounded relative" role="alert">
        <span class="block sm:inline sm:text-xs md:text-base">Already Have an account?</span>
        <strong class="font-bold sm:text-sm md:text-base"><a href="/login">Click here to Login!</a></strong>
        </div>

        </div>
    
@endif
    @unless (Auth::check())
    
    @endunless

    <div id="login-register"></div>
    <div id="register-form"></div>
    
    
    </div>


</div>


</div>

@endif