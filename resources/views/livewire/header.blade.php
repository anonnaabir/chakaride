<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ setting('site.title') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/map.css')}}" />
    <script type="module" src="{{ asset('js/map.js')}}"></script>
    @livewireStyles
</head>
<body>

@if ($checkDevice == false)
<nav>
    <div class="flex bg-white p-2 justify-between items-center w-full">
        <div class="justify-start">
        <a href="/"><img src="{{asset('storage/'. $logoPath)}}" 
        class="mr-3 h-12 w-16" alt="Chakaride Booking" /></a>
        </div>

        <div class="justify-center">
        <a href="https://chakaride.com" class="mr-4 ml-4 text-black">Home</a>
        <a href="https://chakaride.com/about/" class="mr-4 ml-4 text-black">About</a>
        <a href="/" class="mr-4 ml-4 text-black">Booking</a>
        <a href="https://chakaride.com/drivers/" class="mr-4 ml-4 text-black">Drivers</a>
        <a href="https://chakaride.com/faq/" class="mr-4 ml-4 text-black">FAQ</a>
        <a href="https://chakaride.com/contact/" class="mr-4 ml-4 text-black">Contact</a>
        </div>

        <div class="justify-end">
        <a href="/user-area" class="p-4 bg-black text-white rounded">My Account</a>
        </div>
</div>

</nav>

@else

<nav>
    <div class="flex bg-white p-2 justify-between items-center w-full">
        <div class="justify-start">
        <a href="/"><img src="{{asset('storage/'. $logoPath)}}" 
        class="mr-3 h-12 w-16" alt="Chakaride Booking" /></a>
        </div>
        
        <div class="justify-end">
        <a href="/user-area" class="p-4 bg-black text-white rounded">My Account</a>
        </div>
</div>

</nav>
    
@endif