<div class="pt-4" x-show="checkoutStep2">
    
<h1 class="sm:text-2xl md:text-3xl text-black text-center">Order Summary</h1>

<div class="grid place-items-center sm:pt-4 md:pt-4">

<div class="sm:w-full md:w-2/5 text-center">

<p class="sm:text-sm md:text-base text-black pt-2">Booking Type: <span class="font-medium">{{$booking_type}}</span></p>
<p class="sm:text-sm md:text-base text-black pt-2">Trip Type: <span class="font-medium">{{$trip_type}}</span></p>
<p class="sm:text-sm md:text-base text-black pt-2">Pickup Location : <span class="font-medium">{{$pickup_location}}</span></p>
<p class="sm:text-sm md:text-base text-black pt-2">Dropoff Location : <span class="font-medium">{{$dropoff_location}}</span></p>
<p class="sm:text-sm md:text-base text-black pt-2">Pickup Date : <span class="font-medium">{{$pickup_date}}</span></p>
<p class="sm:text-sm md:text-base text-black pt-2">Pickup Time : <span class="font-medium">{{$pickup_time}}</span></p>
@if ($booking_type == 'Daily Basic')
<p class="sm:text-sm md:text-base text-black pt-2">Finish Date : <span class="font-medium">{{$dropoff_date}}</span></p>
@endif
<p class="sm:text-sm md:text-base text-black pt-2">Selected Car : <span class="font-medium">{{$car_model}}</span></p>
<p class="text-black pt-4 text-xl font-medium">Total Cost : {{$total_cost}} TK</p>

<div class="mt-12">
<label class="text-black" for="referance_number">Referance Number</label><br>
<input class="text-black sm:w-full md:w-3/5 mt-4 mb-4 rounded-md" type="text" id="referance_number" name="referance_number" value=""><br>
</div>

</div>

<div class="mt-6 p-6 border-2 border-rose-600 rounded-md text-black text-left sm:w-full md:w-2/5">
    <h1 class="sm:text-base md:text-xl text-center font-medium">Condition And Charges</h1>
    @if ($booking_type == 'Daily Basic')
    <div class="sm:text-sm md:text-base pt-4 pb-4">{!! $booking_condition !!}</div>
    @else
    <div class="sm:text-sm md:text-base pt-4 pb-4">{!! $booking_condition !!}</div>
    <div class="sm:text-sm md:text-base pt-4 pb-4">{!! $extra_charge !!}</div>
    @endif
</div>

</div>

<div class="flex justify-center">

<button type="submit" class="bg-rose-600 rounded-md sm:text-sm md:text-base text-white pt-3 pb-4 sm:pl-8 md:pl-20 sm:pr-8 md:pr-20 mt-8">Confirm Booking</button>
</div>

</div>