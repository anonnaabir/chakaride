<div class="text-black" x-show="step3">
<h1 class="text-2xl text-black text-center mt-4 mb-8">Select Car</h1>
<div class="grid sm:grid-cols-1 md:grid-cols-2 gap-4">
@foreach ($cars as $car)
        <div id="{{ $car->car_model }}" class="{{ $car->car_category }} current-car border-solid border-2 border-rose-300 rounded-md p-4 w-full hidden">

        <div class="grid sm:grid-cols-1 md:grid-cols-2">
            <div class="flex justify-center">
                
            <img class="sm:h-48 sm:w-48 md:h-64 md:w-64" src="{{asset('storage/'.$car->car_image)}}" />
            </div>

            <div>
            <h2 class="sm:text-center text-2xl">{{ $car->car_model }}</h2><br>
            <div class="sm:text-center">{!! $car->car_description !!}</div><br>
            <div class="flex justify-center">
            <button type="button" id="{{ $car->car_model }}" class="car-select bg-black text-white pt-2 pb-2 p-4 rounded-md">Select This Car</button>
            </div>    
        </div>
        
        </div>
        </div>
        
    @endforeach
</div>

<input type="hidden" id="selected_car" name="selected_car" value="">

<div class="flex justify-center">

<button type="button" id="carlist_back" x-on:click="step3 = false,step2 = true"
class="bg-rose-600 text-white pt-3 pb-4 sm:pl-8 md:pl-10 sm:pr-8 md:pr-10 sm:mt-4 md:mt-8 sm:mb-8 md:mb-4 rounded-md">Back</button>

</div>

</div>