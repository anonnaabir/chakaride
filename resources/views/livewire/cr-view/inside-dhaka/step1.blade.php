<div class="cr-pickup-location pt-4" x-show="step1">
    
<h1 class="text-2xl text-black text-center">Enter Trip Details</h1>

<div class="grid place-items-center sm:pt-4 md:pt-4">

<div class="sm:w-full md:w-2/5">
<label class="text-black" for="pickup_input">Pickup Location</label><br>
<input id="pickup_input" name="pickup_input" class="w-full text-black mt-6 mb-6 rounded-md" type="text" placeholder="Search Location"><br>

<label class="text-black" for="pickup_date">Pickup Date</label><br>
<input class="text-black w-full mt-4 mb-4 rounded-md" type="date" id="pickup_date" name="pickup_date" value=""><br>

<label class="text-black" for="pickup_time">Pickup Time</label><br>
<input class="text-black w-full mt-4 mb-4 rounded-md" type="time" id="pickup_time" name="pickup_time" value=""><br><br>

<input type="hidden" id="pickup_lat" name="pickup_lat" value="">
<input type="hidden" id="pickup_lng" name="pickup_lng" value="">

</div>


<div class="sm:w-full md:w-2/5">

<div class="flex justify-between">

<a href="/" class="bg-rose-600 text-white pt-3 pb-4 pl-10 pr-10 mt-8 rounded-md">Back</a>

<button type="button" id="pickup_location_next" x-on:click="step1 = false,step2 = true"
class="bg-rose-600 text-white pt-3 pb-4 pl-10 pr-10 mt-8 rounded-md" disabled>Next</button>

</div>

</div>

</div>

</div>