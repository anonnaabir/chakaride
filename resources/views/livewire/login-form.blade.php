@if (Auth::check())

<div class="cr-booking-type pt-4" x-show="checkoutStep2">
    
<h1 class="text-2xl text-black text-center">Login Form</h1>
<p class="text-black text-center"></p>

<div class="flex justify-center">

<a class="bg-rose-600 text-white pt-3 pb-4 pl-10 pr-10 mt-8" href="/check-auth">Confirm Booking</a>

</div>

</div>

@else

<div class="cr-booking-type pt-4" x-show="checkoutStep2">
    
<h1 class="text-2xl text-black text-center">Register Form</h1>

<form action="/success">
{{ csrf_field() }}
<div class="grid grid-cols-2 place-items-center pt-12 pl-8 pr-8">
      <div class="w-4/5">
      <label class="text-black" for="full_name">Full Name</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="full_name" name="full_name" value=""><br>

      <label class="text-black" for="customer_phone">Phone</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="customer_phone" name="customer_phone" value=""><br>

      <label class="text-black" for="customer_phone">Address</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="customer_address" name="customer_address" value=""><br>
</div>

    <div class="w-4/5">
    <label class="text-black" for="customer_email">Email</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="customer_email" name="customer_email" value=""><br>

    <label class="text-black" for="customer_phone">City</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="customer_city" name="customer_city" value=""><br>

      <label class="text-black" for="customer_pass">Password</label><br>
      <input class="text-black w-full mt-4 mb-4" type="text" id="customer_pass" name="customer_pass" value=""><br>
</div>
</div>


<div class="flex justify-center">

<button type="submit" class="bg-rose-600 text-white pt-3 pb-4 pl-10 pr-10 mt-8">Register</button>

</div>

</form>

</div>
@endif