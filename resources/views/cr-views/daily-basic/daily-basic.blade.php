<livewire:header />

<div class="grid h-screen overflow-auto place-items-center bg-rose-600 text-white w-full"
x-data="
{
 step1: true,
 step2: false,
 step3: false,
 step4: false,
}"
>

<div class="main-container bg-white justify-center sm:p-8 md:p-12 sm:m-8 md:m-0">

<form id="booking_submit" action="/confirm-booking">
{{ csrf_field() }}
<livewire:cr-view.daily-basic.step1 />
<livewire:cr-view.daily-basic.step2 />
<livewire:cr-view.daily-basic.step3 />
</form>

</div>


</div>
<livewire:footer />