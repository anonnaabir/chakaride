<livewire:header />
<div class="grid h-screen overflow-auto place-items-center bg-rose-600 text-white w-full">

<div class="main-container bg-white justify-center sm:p-8 md:p-12 sm:m-8 md:m-0">

<div class="pt-4">
    
<h1 class="sm:text-xl md:text-3xl text-black text-center">Welcome To Chakaride</h1>

<div class="grid place-items-center sm:pt-4 md:pt-4">

<div class="sm:w-full md:w-2/5 text-center">

<a href="/register"><div class="sm:p-12 md:p-24 bg-black text-white m-4">New Profile</div></a>
<a href="/login"><div class="sm:p-12 md:p-24 bg-black text-white m-4">My Account</div></a>

</div>
</div>

</div>

</div>


</div>
<livewire:footer />