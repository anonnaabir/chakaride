<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Profile') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-12 bg-white border-b border-gray-200">
                
                    
                <form method="post" enctype="multipart/form-data" >
                {{ csrf_field() }}

                <div class="grid sm:grid-cols-1 md:grid-cols-2">

                <div class="sm:w-full md:w-3/5">
                    <div class="text-center">
                    <img class="sm:mt-2 md:mt-6 mb-4 sm:ml-4 md:ml-16 sm:h-48 sm:w-48 md:h-48 md:w-48 border border-black-600 rounded-full" src="{{asset('storage/'.$user_data->avatar)}}" />
                    <label for="customer_avatar">Profile Image</label><br>
                    <input type="file" class="w-full sm:ml-4 md:ml-16 mt-6 sm:mb-12 md:mb-4" id="customer_avatar" name="customer_avatar" /><br>
                    </div>
                </div>


                <div class="sm:w-full md:w-3/5">

                <label for="full_name">Name</label><br>
                <input type="text" class="w-full mt-2" id="full_name" name="full_name" value="{{$user_data->name}}"><br><br>

                <label for="customer_email">Email</label><br>
                <input type="text" class="w-full mt-2" id="customer_email" name="customer_email" value="{{$user_data->email}}"><br><br>

                <label for="customer_phone">Phone Number</label><br>
                <input type="text" class="w-full mt-2" id="customer_phone" name="customer_phone" value="{{$user_data->phone}}"><br><br>

                <label for="customer_address">Address</label><br>
                <input type="text" class="w-full mt-2" id="customer_address" name="customer_address" value="{{$user_data->address}}"><br><br>

                <label for="customer_nid">National ID</label><br>
                <input type="text" class="w-full mt-2" id="customer_nid" name="customer_nid" value="{{$user_data->national_id}}"><br><br>

                <label for="customer_birthdate">Date of Birth</label><br>
                <input type="date" class="w-full mt-2" id="customer_birthdate" name="customer_birthdate" value="{{$user_data->date_of_birth}}"><br><br>

                <label for="customer_password">Password</label><br>
                <input type="password" class="w-full mt-2" id="customer_password" name="customer_password" value="{{$user_data->password}}"><br><br>
                
                <button type="submit" class="bg-black text-white p-4 sm:w-full">Update Profile</button>
                </div>

                </div>

                </form>
               

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
