require('./bootstrap');
window.$ = require('jquery');

import Vue from 'vue';
import App from './App.vue';
import Register from './Register.vue';
import Alpine from 'alpinejs';
import Swal from 'sweetalert2';
import "@lottiefiles/lottie-player";


window.Alpine = Alpine;

Alpine.start();


$('.cr-trip-type').on("click", function(event){
    var str = event.target.id;
    $(str).css("background-color","red");
    $('input[name="booking_type"]').attr('value',str);
    $('#step_1').prop("disabled",false);
});


$('.cr-car-type').on("click", function(event){
  var str = event.target.id;
  $(str).css("background-color","red");
  $('input[name="car_type"]').attr('value',str);
  var car_type = $('input[name="car_type"]').val();
  $('.' + car_type).show();
});

$('#carlist_back').on("click", function(){
  var car_type = $('input[name="car_type"]').val();
  $('.' + car_type).hide();  
});

$('input[name="pickup_input"]').change(function() {
  console.log('Changed');
  $('#pickup_location_next').prop("disabled",false);
});

$('input[name="dropoff_input"]').change(function() {
  console.log('Changed');
  $('#dropoff_location_next').prop("disabled",false);
});

$('button').on('click', function(){
    $('button').removeClass('selected');
    $(this).addClass('selected');
});

$('.car-select').on("click", function(event){
    var str = event.target.id;
    $('input[name="selected_car"]').attr('value',str);
    // Swal.fire(
    //   'Great',
    //   'You have selected ' + str,
    //   'success'
    // )

    Swal.fire({
      title: 'Great',
      text: 'You have selected ' + str + '. Do you confirm this car?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $( "#booking_submit" ).submit();
      }
    });

});



new Vue({
  el: '#login-register',
  render: h => h(App)
})

new Vue({
  el: '#register-form',
  render: h => h(Register)
})
 