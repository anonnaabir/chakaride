/**
 * @license
 * Copyright 2019 Google LLC. All Rights Reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
// @ts-nocheck TODO remove when fixed
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initAutocomplete() {

  var options = {
    componentRestrictions: {country: "bd"}
   };
  
   var pickupInput = document.getElementById('pickup_input');
   var pickupAutocomplete = new google.maps.places.Autocomplete(pickupInput, options);

   google.maps.event.addListener(pickupAutocomplete, 'place_changed', function () {
    var place = pickupAutocomplete.getPlace();
    // console.log(place.geometry.location.lat());
    // console.log(place.geometry.location.lng());
    var cr_lat = place.geometry.location.lat()
    var cr_lng = place.geometry.location.lng()
    $('input[name="pickup_lat"]').attr('value',cr_lat);
    $('input[name="pickup_lng"]').attr('value',cr_lng);

  });


  var dropoffInput = document.getElementById('dropoff_input');
  var dropoffAutocomplete = new google.maps.places.Autocomplete(dropoffInput, options);


  google.maps.event.addListener(dropoffAutocomplete, 'place_changed', function () {
    var place = dropoffAutocomplete.getPlace();
    // console.log(place.geometry.location.lat());
    // console.log(place.geometry.location.lng());
    var cr_lat = place.geometry.location.lat()
    var cr_lng = place.geometry.location.lng()
    $('input[name="dropoff_lat"]').attr('value',cr_lat);
    $('input[name="dropoff_lng"]').attr('value',cr_lng);

  });

}

window.initAutocomplete = initAutocomplete;
