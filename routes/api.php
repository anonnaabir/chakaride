<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\CustomerDataApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/customer-list', 'App\Http\Controllers\CustomerData@get_customer_data_api');

Route::post('/login', [AuthController::class, 'login']);

Route::post('/me', [AuthController::class, 'me']);

Route::get('/users/phone/{phone}', [CustomerDataApiController::class, 'check_phone']);
Route::get('/users/nid/{nid}', [CustomerDataApiController::class, 'check_nid']);
Route::get('/users/email/{email}', [CustomerDataApiController::class, 'check_email']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
