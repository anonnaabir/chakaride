<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['prefix' => '/secret/admin'], function () {
    Voyager::routes();
});

Route::get('/customer', 'App\Http\Controllers\CustomerData@get_customer_data');

// Route::get('/booking', 'App\Http\Controllers\Booking@booking_form');
Route::get('/', 'App\Http\Controllers\Booking@booking_type');
Route::get('/booking', 'App\Http\Controllers\Booking@car_booking');
Route::get('/car-list', 'App\Http\Controllers\Booking@car_list');
Route::get('/confirm-booking', 'App\Http\Controllers\Booking@store_session');
Route::get('/cancel-booking', 'App\Http\Controllers\Booking@cancelBooking');
Route::get('/success', 'App\Http\Controllers\Booking@store');
Route::get('/reg-success', 'App\Http\Controllers\Booking@regSucess');
Route::get('/check-auth', 'App\Http\Controllers\Booking@checkAuth');
Route::get('/all-bookings', 'App\Http\Controllers\CustomerData@get_bookings');
Route::get('/my-profile', 'App\Http\Controllers\CustomerData@profile_data');
Route::post('/my-profile', 'App\Http\Controllers\CustomerData@update_profile');

Route::get('/user-area', function () {
    return view('user-area');
});

Route::get('/cr-login', function () {
    return view('login');
});

Route::post('/check-login', 'App\Http\Controllers\CustomerData@login_ar');

Route::get('dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
