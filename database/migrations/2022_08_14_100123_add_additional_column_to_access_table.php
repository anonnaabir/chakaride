<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_access_tokens', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->string('token')->nullable();
            $table->string('abilities')->nullable();
            $table->string('tokenable_id')->nullable();
            $table->string('tokenable_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personal_access_tokens', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('token');
            $table->dropColumn('abilities');
            $table->dropColumn('tokenable_id');
            $table->dropColumn('tokenable_type');
        });
    }
};
