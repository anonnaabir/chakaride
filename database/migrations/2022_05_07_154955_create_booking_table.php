<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->text('booking_type');
            $table->text('trip_type');
            $table->text('car_brand');
            $table->string('car_model');
            $table->date('pickup_date')->nullable();
            $table->time('pickup_time')->nullable();
            $table->date('dropoff_date')->nullable();
            $table->time('dropoff_time')->nullable();
            $table->string('pickup_location')->nullable();
            $table->string('dropoff_location')->nullable();
            $table->integer('total_distance');
            $table->integer('total_cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
};
