<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RentChartModel extends Model
{
    use HasFactory;
    protected $table = 'rent_chart';
    protected $primaryKey = 'id';
}
