<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarCategoryModel extends Model
{
    use HasFactory;
    protected $table = 'car_category';
    protected $primaryKey = 'id';
}
