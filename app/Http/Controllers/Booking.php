<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

use Illuminate\Support\Facades\Notification;
use App\Notifications\BookingSucess;
use App\Notifications\CreateUserSucess;
use App\Notifications\AdminEmail;

use App\Models\BookingModel;
use App\Models\RentChartModel;
use App\Models\CarsModel;
use App\Models\CarCategoryModel;
use App\Models\User;


class Booking extends Controller {
    
    public function booking_form() {
        return view('booking');
    }

    public function booking_type() {
        $rent_chart = RentChartModel::all();
        
        return view('booking-type',[
            'rent_chart' => $rent_chart
        ]);
    }

    public function car_list(Request $request) {
        $booking_type = $request->booking_type;
        // $car_category = $request->car_type;
        // $car_category = 'sedan_car';
        // $cars = CarsModel::where('id',1)->first();
        // dd($cars->car_description);
        $booking_condition = RentChartModel::where('booking_type','Inside Dhaka')->first();
        // dd($booking_condition->booking_condition);
        return view('car-list',[
            'cars' => $cars
        ]);
    }

    public function car_booking(Request $request) {
        $booking_type = $request->booking_type;
        Session::put('booking_type',$booking_type);
        $selected_booking = Session::get('booking_type');
        // dd($selected_booking);
        
        if ($selected_booking == 'Inside Dhaka') {
            return view('cr-views/inside-dhaka/inside-dhaka-booking');
        }

        if ($selected_booking == 'Outside Dhaka') {
            return view('cr-views/outside-dhaka/outside-dhaka-booking');
        }

        if ($selected_booking == 'Airport Booking') {
            return view('cr-views/airport-booking/airport-booking');
        }

        if ($selected_booking == 'Daily Basic') {
            return view('cr-views/daily-basic/daily-basic');
        }

    }


    public function regSucess(Request $request) {
        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'phone', 'max:20', 'unique:users'],
            'national_id' => ['required', 'string', 'phone', 'max:50', 'unique:users'],
        ]);

        User::create([
            'role_id' => 2,
            'name' => $request->full_name,
            'email' => $request->customer_email,
            'password' => Hash::make($request->customer_pass),
            'national_id' => $request->national_id,
            'phone' => $request->customer_phone,
        ]);
    }

    public function store(Request $request) {
        $booking = new BookingModel;
        
        $confirm_booking = Session::get('booking_data');

        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }

        else {

            $validated = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'string', 'max:20', 'unique:users'],
                'national_id' => ['required', 'string', 'max:50', 'unique:users'],
                'password' => ['required', Rules\Password::defaults()],
            ]);

            $createUser = User::create([
                'role_id' => 2,
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->customer_pass),
                'phone' => $request->phone,
                'national_id' => $request->national_id,
                'address' => $request->address,
            ]);

            $user_id = $createUser->id;
            $user_details = $createUser;

            $user = User::find($user_id);
            $user->notify(new CreateUserSucess($user_details));
        }
        
        $booking->user_id = $user_id;
        $booking->booking_type = $confirm_booking['booking_type'];
        $booking->trip_type = $confirm_booking['trip_type'] ?? 'Not Available';
        $booking->car_brand = $confirm_booking['car_brand'];
        $booking->car_model = $confirm_booking['car_model'];
        $booking->pickup_location = $confirm_booking['pickup_location'];
        $booking->dropoff_location = $confirm_booking['dropoff_location'] ?? 'Not Available';
        $booking->pickup_date = $confirm_booking['pickup_date'];
        $booking->pickup_time = $confirm_booking['pickup_time'];
        $booking->dropoff_date = ($confirm_booking['booking_type'] == 'Daily Basic') ? $confirm_booking['finish_date']->format('d-m-y') : null;
        $booking->total_distance = $confirm_booking['total_distance'];
        $booking->total_cost = $confirm_booking['total_cost'];
        $booking->customer_id = $user_id;
        $booking->referance_number = $request->referance_number ?? 'Not Available';
        $booking->save();


        $booking_data = BookingModel::latest('id')->first();
        // dd($booking_id);

        $user = User::find($user_id);
        // dd($user);
        $admin_email = setting('admin.admin_email');
        

        $user->notify(new BookingSucess($booking_data));
        Notification::route('mail', $admin_email)->notify(new AdminEmail($booking_data,$user));

        return view('booking-success',[
            'booking_id' => $booking_data->id
        ]);
    }

    public function store_session(Request $request) {
        $booking_type = Session::get('booking_type');
        $trip_type = $request->trip_type;
        
        $selected_car = $request->selected_car;

        $rent_chart = RentChartModel::where('booking_type',$booking_type)->first();
        $car_data = CarsModel::where('car_model',$selected_car)->first();

        $pickup_lat = $request->pickup_lat;
        $pickup_lng = $request->pickup_lng;
        $dropoff_lat = $request->dropoff_lat;
        $dropoff_lng =$request->dropoff_lng;
        $apiKey = 'AIzaSyDvuQmiWX6MNZqg0j-x8CpCcgoGMm0RpEI';

        $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$pickup_lat.','.$pickup_lng.'&destinations='.$dropoff_lat.','.$dropoff_lng.'&key='.$apiKey.'');
        $final_response = json_decode($response,true);
        $reponse_status = $final_response['rows'][0]['elements'][0]['status'];

        if ($reponse_status == 'ZERO_RESULTS') {
            $total_distance = 0;
        }

        else {
            $total_distance = ($final_response['rows'][0]['elements'][0]['distance']['value'])/1000;
        }

        
        if ($booking_type == 'Inside Dhaka') {
            $inside_dhaka_fuel_cost = $car_data->fuel_cost_inside_dhaka;
            $distance_cost = 80*$inside_dhaka_fuel_cost;

            $total_cost = $car_data->base_cost + $distance_cost;
        }


        if ($booking_type == 'Outside Dhaka') {
            if($trip_type == 'one-way') {
                $fuel_cost = $car_data->fuel_cost_outside_dhaka_one;
                $distance_cost = $fuel_cost*($total_distance < 50 ? $total_distance=50 : $total_distance);
                $final_cost = $distance_cost + $car_data->base_cost;
            }

            if ($trip_type == 'round-trip') {
                $fuel_cost = $car_data->fuel_cost_outside_dhaka_round;
                $distance_cost = $fuel_cost*($total_distance < 50 ? $total_distance=50 : $total_distance);
                $final_cost = $distance_cost + $car_data->base_cost;
            }

            $total_cost = $final_cost;

        }


        if ($booking_type == 'Airport Booking') {
            if($trip_type == 'one-way') {
                $airport_charge =  $car_data->fuel_cost_airport_one;
                $final_cost = $car_data->base_cost*$airport_charge/100;        
            }

            if ($trip_type == 'round-trip') {
                $airport_charge =  $car_data->fuel_cost_airport_round;
                $final_cost = $car_data->base_cost*$airport_charge/100;
            }

            $total_cost = $final_cost;

        }


        if ($booking_type == 'Daily Basic') {
            if($trip_type == 'inside-dhaka') {
                $pickup_date = date_create($request->pickup_date);
                $finish_date = date_create($request->finish_date);

                $trip_duration=date_diff($pickup_date,$finish_date);

                $booking_charge =  $car_data->fuel_daily_basic_inside_dhaka;
                $booking_cost = $car_data->base_cost + $car_data->base_cost*$booking_charge/100;

                $final_cost = $booking_cost*($trip_duration->days + 1);
            }

            if ($trip_type == 'outside-dhaka') {
                $pickup_date = date_create($request->pickup_date);
                $finish_date = date_create($request->finish_date);

                $trip_duration=date_diff($pickup_date,$finish_date);

                $booking_charge =  $car_data->fuel_daily_basic_outside_dhaka;
                $booking_cost = $car_data->base_cost + $car_data->base_cost*$booking_charge/100;

                $final_cost = $booking_cost*($trip_duration->days + 1);
            }

            $total_cost = $final_cost;

        }

        $booking_data = [
            'booking_type' => $booking_type,
            'trip_type' => $request->trip_type,
            'car_brand' => $car_data->car_brand,
            'car_model' => $car_data->car_model,
            'pickup_date' => date_create($request->pickup_date),
            // 'dropoff_date' => date_create($request->dropoff_date),
            'finish_date' => ($booking_type == 'Daily Basic') ? date_create($request->finish_date) : "Not Available",
            'pickup_time' => $request->pickup_time,
            'pickup_location' => $request->pickup_input,
            'dropoff_location' => $request->dropoff_input,
            // 'trip_duration' => $trip_duration,
            'total_distance' => $total_distance,
            'total_cost' => $total_cost,
        ];

        Session::put('booking_data',$booking_data);
        $confirm_booking = Session::get('booking_data');

        // Session::flush();
        // dd($confirm_booking['pickup_date']->format('d-m-y'));

        // return view('confirm-booking',[
        //     'booking_type' => $confirm_booking['booking_type'],
        //     'trip_type' => $confirm_booking['trip_type'],
        //     'car_brand' => $confirm_booking['car_brand'],
        //     'car_model' => $confirm_booking['car_model'],
        //     'pickup_date' => $confirm_booking['pickup_date']->format('d-m-y'),
        //     'total_distance' => $confirm_booking['total_distance'],
        //     'total_cost' => $confirm_booking['total_cost'],
        //     'car_brand' => $confirm_booking['car_brand'],
        // ]);

        return view('confirm-booking');

        
    }

    public function cancelBooking() {
        Session::flush();

    }


    public function checkAuth() {
        $user_id = Auth::user();
        if ($user_id == null) {
            return view('auth/login'); 
        }      
        
    }
}
