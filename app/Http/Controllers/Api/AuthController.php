<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\AccessTokenModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller {
    
    public function login(Request $request) {

        if (!\Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }
    
        $user = User::where('email', $request['email'])->firstOrFail();
    
        $token = $user->createToken('remember_token')->plainTextToken;

        $user->remember_token = $token;

        $user->save();
    
        return response()->json([
                'remember_token' => $token,
                // 'token_type' => 'Bearer',
        ]);
            
    }

    public function me(Request $request) {
        $user = Auth::user();
        return $user;
        // return $request->user();
    }

}
