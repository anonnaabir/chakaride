<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerDataApiController extends Controller {

    public function check_phone($phone){
        $user = User::where('phone',$phone)->first();
        if($user){
            return response()->json([
                'status' => false,
                'message' => 'Phone Number Already Exists'
            ]);
        }
    }

    public function check_nid($nid){
        $user = User::where('national_id',$nid)->first();
        if($user){
            return response()->json([
                'status' => false,
                'message' => 'National ID Already Exists'
            ]);
        }
    }

    public function check_email($email){
        $user = User::where('email',$email)->first();
        if($user){
            return response()->json([
                'status' => false,
                'message' => 'Email Already Exists'
            ]);
        }
    }

}
