<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\BookingModel;

class CustomerData extends Controller
{
    public function get_customer_data() {
        $id = Auth::user()->id;
        $name = Auth::user()->name;
        $email = Auth::user()->email;
        $phone = Auth::user()->phone;

        $order_details = BookingModel::where('user_id',"=", $id)->first();
        // dd($order_details);
        
        return view('customerdata', [
            'name' => $name,
            'email' => $email,
            'phone' => '0'.$phone,
            'booking_type' => $order_details->booking_type,
            'car_brand' => $order_details->car_brand,
        ]);
    }


    public function get_bookings() {
        
        $id = Auth::user()->id;

        // $bookings = BookingModel::where('user_id',"=", $id)->first();
        $bookings = BookingModel::where('user_id',$id)->get();
        // dd($bookings[0]->booking_type);
        return view('bookings',[
            'booking_data' => $bookings,
        ]);
    }

    public function profile_data() {
        
        $id = Auth::user()->id;

        $user_data = User::where('id',$id)->first();
        // dd($user_data);
        return view('my-profile',[
            'user_data' => $user_data,
        ]);
    }


    public function update_profile(Request $request) {
        $id = Auth::user()->id;
        $user_data = User::where('id',$id)->first();
        $user = new User;

        if ($request->hasFile('customer_avatar') == null) {
            $userAvatar = $user_data->avatar;
        }

        else {
            $userAvatar = $request->file('customer_avatar')->store('users');
        }

        // $userAvatar = $request->hasFile('customer_avatar');
        // dd($userAvatar);
        // dd($userData->password);
        // $user_data = User::where('id',$id)->first();

        // $user->name = $request->full_name;
        // $user->email = $request->customer_email;
        // $user->password = $userData->password;
        // $booking->booking_type = $confirm_booking['booking_type'];
        // $booking->trip_type = $confirm_booking['trip_type'];
        // $booking->car_brand = $confirm_booking['car_brand'];
        // $booking->car_model = $confirm_booking['car_model'];
        // $booking->total_distance = $confirm_booking['total_distance'];
        // $booking->total_cost = $confirm_booking['total_cost'];
        // $booking->customer_id = $user_id;

        $user->where('id',$id)->update([
            'name' => $request->full_name,
            'email' => $request->customer_email,
            'avatar' => $userAvatar,
            'password' => Hash::make($request->customer_password),
            'date_of_birth' => $request->customer_birthdate,
            'national_id' => $request->customer_nid,
            'address' => $request->customer_address
         ]);

         return redirect()->back();

    }


    public function login_ar(Request $request) {
        $credentials = $request->only('email', 'password');
        $confirm_booking = Session::get('booking_data');
        
        if (Auth::attempt($credentials)) {
            if ($confirm_booking != null) {
                return redirect()->intended('confirm-booking')
                        ->withSuccess('You have Successfully loggedin');
            }

            else {
                return redirect()->intended('/')
                        ->withSuccess('You have Successfully loggedin');
            }
            
        }
  
        return redirect("cr-login")->withSuccess('Oppes! You have entered invalid credentials');
        // return view('login');
    }

    public function get_customer_data_api() {
        // $id = Auth::user()->id;
        // $name = Auth::user()->name;
        // $email = Auth::user()->email;
        // $phone = Auth::user()->phone;

        // return $name;
        $users = User::all();
        return $users;
        // return response()->json($name);
        // $order_details = BookingModel::where('user_id',"=", $id)->first();
    }
}
