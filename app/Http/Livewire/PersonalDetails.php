<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Livewire\Component;

class PersonalDetails extends Component {
    public function render() {
        $confirm_booking = Session::get('booking_data');
        // dd($confirm_booking);
        if (Auth::check()) {
            $user = Auth::user()->id;
            $user_data = User::where('id',$user)->first();
        }

        else {
            $user_data = null;
        }
        
        // dd($confirm_booking['pickup_date']->format('d-m-y'));
        return view('livewire.personal-details', [
            'user_data' => $user_data,
            'booking_type' => $confirm_booking['booking_type'],
            'trip_type' => $confirm_booking['trip_type'],
            'car_brand' => $confirm_booking['car_brand'],
            'car_model' => $confirm_booking['car_model'],
            'pickup_date' => $confirm_booking['pickup_date']->format('d-m-y'),
            'total_distance' => $confirm_booking['total_distance'],
            'total_cost' => $confirm_booking['total_cost'],
        ]);
    }
}
