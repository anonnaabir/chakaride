<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CarBooking extends Component
{
    public function render()
    {
        return view('livewire.car-booking');
    }
}
