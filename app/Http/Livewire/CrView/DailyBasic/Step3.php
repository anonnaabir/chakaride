<?php

namespace App\Http\Livewire\CrView\DailyBasic;
use App\Models\CarsModel;
use Livewire\Component;

class Step3 extends Component
{
    public function render() {

        $cars = CarsModel::all();
        // dd($cars);
        return view('livewire.cr-view.daily-basic.step3',[
            'cars' => $cars
        ]);
    }
}
