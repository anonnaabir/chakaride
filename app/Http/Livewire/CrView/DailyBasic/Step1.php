<?php

namespace App\Http\Livewire\CrView\DailyBasic;

use Livewire\Component;

class Step1 extends Component
{
    public function render()
    {
        return view('livewire.cr-view.daily-basic.step1');
    }
}
