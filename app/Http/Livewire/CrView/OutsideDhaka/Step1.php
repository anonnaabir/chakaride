<?php

namespace App\Http\Livewire\CrView\OutsideDhaka;

use Livewire\Component;

class Step1 extends Component
{
    public function render()
    {
        return view('livewire.cr-view.outside-dhaka.step1');
    }
}
