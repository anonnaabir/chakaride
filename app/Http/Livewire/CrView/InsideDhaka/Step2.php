<?php

namespace App\Http\Livewire\CrView\InsideDhaka;
use App\Models\CarCategoryModel;
use Livewire\Component;

class Step2 extends Component
{
    public function render() {
        $car_category = CarCategoryModel::all();
        // dd($car_category[1]->category_name);
        return view('livewire.cr-view.inside-dhaka.step2',[
            'car_category' => $car_category
        ]);
    }
}
