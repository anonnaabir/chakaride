<?php

namespace App\Http\Livewire\CrView\InsideDhaka;

use Livewire\Component;

class Step1 extends Component
{
    public function render()
    {
        return view('livewire.cr-view.inside-dhaka.step1');
    }
}
