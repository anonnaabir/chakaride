<?php

namespace App\Http\Livewire\CrView\InsideDhaka;
use Illuminate\Http\Request;
use App\Models\CarsModel;
use App\Models\RentChartModel;
use Livewire\Component;

class Step3 extends Component
{
    public function render(Request $request) {
        // $booking_type = $request->booking_type;
        // $car_category = $request->car_type;
        // $cars = CarsModel::where('car_category',$car_category)->first();
        // dd($cars);
        // $booking_condition = RentChartModel::where('booking_condition',$booking_type)->get();
        // dd($booking_condition);
        $cars = CarsModel::all();
        return view('livewire.cr-view.inside-dhaka.step3',[
            'cars' => $cars
        ]);
    }
}
