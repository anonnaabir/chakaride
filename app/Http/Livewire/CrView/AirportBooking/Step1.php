<?php

namespace App\Http\Livewire\CrView\AirportBooking;

use Livewire\Component;

class Step1 extends Component
{
    public function render()
    {
        return view('livewire.cr-view.airport-booking.step1');
    }
}
