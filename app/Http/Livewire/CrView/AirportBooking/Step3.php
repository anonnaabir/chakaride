<?php

namespace App\Http\Livewire\CrView\AirportBooking;
use App\Models\CarsModel;
use Livewire\Component;

class Step3 extends Component {

    public function render() {

        $cars = CarsModel::all();
        return view('livewire.cr-view.airport-booking.step3',[
            'cars' => $cars
        ]);
    }
}
