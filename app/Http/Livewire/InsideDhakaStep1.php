<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InsideDhakaStep1 extends Component
{
    public function render()
    {
        return view('livewire.inside-dhaka-step1');
    }
}
