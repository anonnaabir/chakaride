<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BookingStep2 extends Component
{
    public function render()
    {
        return view('livewire.booking-step2');
    }
}
