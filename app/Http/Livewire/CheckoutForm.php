<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;

use Livewire\Component;

class CheckoutForm extends Component {
    
    public function render() {

        $user_id = Auth::user();

        return view('livewire.checkout-form',[
            'user' => $user_id
        ]);
    }

}
