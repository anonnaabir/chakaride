<?php

namespace App\Http\Livewire;
use App\Models\RentChartModel;

use Livewire\Component;

class Booking1 extends Component {

    public function render() {
        $rent_chart = RentChartModel::all();
        // dd($rent_chart);
        return view('livewire.booking1',[
            'rent_chart' => $rent_chart
        ]);
    }
}

