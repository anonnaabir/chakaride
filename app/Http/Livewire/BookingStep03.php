<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BookingStep03 extends Component
{
    public function render()
    {
        return view('livewire.booking-step03');
    }
}
