<?php

namespace App\Http\Livewire;
use Jenssegers\Agent\Agent;
use Livewire\Component;

class Header extends Component
{
    public function render() {
        // dd(setting('site.logo'));
        $agent = new Agent();
        $checkDevice = $agent->isMobile();
        return view('livewire.header',[
            'checkDevice' => $checkDevice,
            'logoPath' => setting('site.logo')
        ]);
    }
}
