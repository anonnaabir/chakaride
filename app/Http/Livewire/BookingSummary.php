<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RentChartModel;
use App\Models\CarCategoryModel;
use Livewire\Component;

class BookingSummary extends Component
{
    public function render(Request $request) {
        $confirm_booking = Session::get('booking_data');
        $booking_type = $confirm_booking['booking_type'];
        $car_category = $request->car_type;
        
        $booking_condition = RentChartModel::where('booking_type',$booking_type)->first();
        $extra_charge = CarCategoryModel::where('category_slug',$car_category)->first();

        if (Auth::check()) {
            $user = Auth::user()->id;
            $user_data = User::where('id',$user)->first();
        }

        else {
            $user_data = null;
        }
        
        // dd();
        return view('livewire.booking-summary', [
            'user_data' => $user_data,
            'booking_type' => $confirm_booking['booking_type'],
            'trip_type' => $confirm_booking['trip_type'] ?? 'Not Available',
            'car_brand' => $confirm_booking['car_brand'],
            'car_model' => $confirm_booking['car_model'],
            'pickup_date' => $confirm_booking['pickup_date']->format('d-m-y'),
            'pickup_time' => date("g:iA", strtotime($confirm_booking['pickup_time'])),
            'dropoff_date' => ($confirm_booking['booking_type'] == 'Daily Basic') ? $confirm_booking['finish_date']->format('d-m-y') : 'Not Available',
            'pickup_location' => $confirm_booking['pickup_location'],
            'dropoff_location' => $confirm_booking['dropoff_location'] ?? 'Not Available',
            'total_distance' => $confirm_booking['total_distance'],
            'total_cost' => $confirm_booking['total_cost'],
            'booking_condition' => $booking_condition->booking_condition,
            'extra_charge' => $extra_charge->extra_charge,
        ]);
    }
}
