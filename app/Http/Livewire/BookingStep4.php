<?php

namespace App\Http\Livewire;
use App\Models\CarsModel;
use Livewire\Component;

class BookingStep4 extends Component
{
    public function render()
    {
        $cars = CarsModel::all();
        $path = storage_path();
        // dd($path);
        // dd($cars);
        return view('livewire.booking-step4',[
            'cars' => $cars
        ]);
    }
}
