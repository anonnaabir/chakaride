<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingSucess extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($booking_data) {
        $this->booking_data = $booking_data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                    ->subject('Congratulation! Your booking has received sucessfully.')
                    ->greeting('Congratulation!')
                    ->line('Your booking has received sucessfully.')
                    ->line('Your booking id: ' . $this->booking_data['id'])
                    ->line('Booking Type: ' . $this->booking_data['booking_type'])
                    ->line('Trip Type: ' . $this->booking_data['trip_type'])
                    ->line('Pickup Location: ' . $this->booking_data['pickup_location'])
                    ->line('Dropoff Location: ' . $this->booking_data['dropoff_location'])
                    ->line('Pickup Date: ' . $this->booking_data['pickup_date'])
                    ->line('Pickup Time: ' . $this->booking_data['pickup_time'])
                    ->line(($this->booking_data['booking_type'] == 'Daily Basic') ? 'Finish Date: '.$this->booking_data['dropoff_date'] : 'Dropoff Date: Not Available')
                    ->line('Selected Car: ' . $this->booking_data['car_brand'].' '.$this->booking_data['car_model'])
                    ->line('Referance Number: ' . $this->booking_data['referance_number'])
                    ->line('Total Cost: ' . $this->booking_data['total_cost'] . ' TK')
                    ->line('Click the button to check your booking details and status')
                    ->action('Check Details', url('/dashboard'))
                    ->line('Thank you for using Chakaride');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
