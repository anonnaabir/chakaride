<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdminEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($booking_data,$user){
        $this->booking_data = $booking_data;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Booking Placed. Booking ID: '. $this->booking_data['id'])
                    ->greeting('Here is the booking details:')
                    ->line('Booking Information:')
                    ->line('Booking id: ' . $this->booking_data['id'])
                    ->line('Booking Type: ' . $this->booking_data['booking_type'])
                    ->line('Trip Type: ' . $this->booking_data['trip_type'])
                    ->line('Pickup Location: ' . $this->booking_data['pickup_location'])
                    ->line('Dropoff Location: ' . $this->booking_data['dropoff_location'])
                    ->line('Pickup Date: ' . $this->booking_data['pickup_date'])
                    ->line('Pickup Time: ' . $this->booking_data['pickup_time'])
                    ->line(($this->booking_data['booking_type'] == 'Daily Basic') ? 'Finish Date: '.$this->booking_data['dropoff_date'] : 'Dropoff Date: Not Available')
                    ->line('Selected Car: ' .$this->booking_data['car_model'])
                    ->line('Referance Number: ' . $this->booking_data['referance_number'])
                    ->line('Total Distance: ' . $this->booking_data['total_distance'] . ' KM')
                    ->line('Total Cost: ' . $this->booking_data['total_cost'] . ' TK')
                    ->line('---------------------')
                    ->line('Customer Information:')
                    ->line('Customer Name: ' . $this->user['name'])
                    ->line('Customer Phone: ' . $this->user['phone'])
                    ->line('Customer Email: ' . $this->user['email'])
                    ->line('Customer NID: ' . $this->user['national_id'])
                    ->line('Customer Address: ' . $this->user['address']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
